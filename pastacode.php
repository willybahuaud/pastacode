<?php
/**
 * Plugin Name:       Pastacode2
 * Description:       Example block scaffolded with Create Block tool.
 * Requires at least: 5.9
 * Requires PHP:      7.0
 * Version:           0.1.0
 * Author:            The WordPress Contributors
 * License:           GPL-2.0-or-later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       pastacode
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

define( 'PASTACODE_VERSION', time() );

/**
 * Registers the block using the metadata loaded from the `block.json` file.
 * Behind the scenes, it registers also all assets so they can be enqueued
 * through the block editor in the corresponding context.
 *
 * @see https://developer.wordpress.org/reference/functions/register_block_type/
 */
function wab_pastacode_block_init() {
	register_block_type(
		__DIR__ . '/build',
		array(
			'render_callback' => 'wab_pastacode_render_callback',
		)
	);
}
add_action( 'init', 'wab_pastacode_block_init' );

/**
 * Render callback function.
 *
 * @param array    $attributes The block attributes.
 * @param string   $content    The block content.
 * @param WP_Block $block      Block instance.
 *
 * @return string The rendered output.
 */
function wab_pastacode_render_callback( $attributes, $content, $block ) {
	ob_start();
	require plugin_dir_path( __FILE__ ) . 'build/template.php';
	return ob_get_clean();
}

require_once plugin_dir_path( __FILE__ ) . 'pastacode-options.php';
require_once plugin_dir_path( __FILE__ ) . 'front.php';
require_once plugin_dir_path( __FILE__ ) . 'remote-code-getter.php';
require_once plugin_dir_path( __FILE__ ) . 'sources.php';
require_once plugin_dir_path( __FILE__ ) . 'settings.php';
require_once plugin_dir_path( __FILE__ ) . 'back-dependencies.php';
// Legacy
require_once plugin_dir_path( __FILE__ ) . 'tinymce-config.php';
require_once plugin_dir_path( __FILE__ ) . 'shortcode.php';

add_action( 'plugins_loaded', 'pastacode_load_languages' );
function pastacode_load_languages() {
	load_plugin_textdomain( 'pastacode', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
}
