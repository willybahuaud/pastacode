import React from 'react';
import CodeMirror from '@uiw/react-codemirror';
import { useState, useEffect } from '@wordpress/element';
import { StreamLanguage } from "@codemirror/language";
import { sass } from "@codemirror/legacy-modes/mode/sass";
import { ruby } from "@codemirror/legacy-modes/mode/ruby";
import { csharp, c } from "@codemirror/legacy-modes/mode/clike";
import { shell } from "@codemirror/legacy-modes/mode/shell";
import { coffeeScript } from "@codemirror/legacy-modes/mode/coffeescript";
import { haskell } from "@codemirror/legacy-modes/mode/haskell";
import { css, less } from "@codemirror/legacy-modes/mode/css";
import { php } from '@codemirror/lang-php';
import { javascript } from '@codemirror/lang-javascript';
import { html } from '@codemirror/lang-html';
import { cpp } from '@codemirror/lang-cpp';
import { python } from '@codemirror/lang-python';
import { markdown } from '@codemirror/lang-markdown';
import { sql } from '@codemirror/lang-sql';
// import { css } from '@codemirror/lang-css';
import { java } from '@codemirror/lang-java';
import { xml } from '@codemirror/lang-xml';

import { createTheme } from '@uiw/codemirror-themes';
import { tags as t } from '@lezer/highlight';

const myTheme = createTheme({
  theme: 'dark',
  settings: {
    background: '#f5f2f0',
    foreground: '#000',
    caret: '#5d00ff',
    selection: 'lightblue',
    selectionMatch: '#036dd626',
    lineHighlight: '#8a91991a',
    gutterBackground: '#f5f2f0',
    gutterForeground: '#999',
	gutterBorder:'#999',
  },
  styles: [
	//e90
    { tag: [t.variableName,t.special(t.propertyName),t.meta], color: '#e90' },
	// grey
    { tag: t.comment, color: 'slategray' },
	// 999
    { tag: [t.brace,t.paren,t.squareBracket,t.punctuation], color: '#999' },
	// opacity .7
	// 905
    { tag: t.bool, color: '#905' },
    { tag: t.number, color: '#905' },
    { tag: t.null, color: '#905' },
    { tag: t.tagName, color: '#905' },
	// 690
    { tag: t.string, color: '#690' },
    { tag: t.attributeName, color: '#690' },
	//color: #a67f59;	background: hsla(0, 0%, 100%, .5);
    { tag: t.operator, color: '#a67f59',backgroundColor:'#ffffff80' },
	//07A
    { tag: t.keyword, color: '#07a' },
	//DD4A68
    // { tag: [t.function(t.variableName)], color: '#DD4A68' },
    // { tag: t.className, color: '#5c6166' },
    // { tag: t.definition(t.typeName), color: '#5c6166' },
    // { tag: t.angleBracket, color: '#5c6166' },
  ],
})


export default function CodePanel (props) {

	const [lang, setLang] = useState( false );

	useEffect( () => {
		let lang;
		switch( props.language ) {
			case 'php':
				lang = php({plain:true});
				break;
			case 'cpp':
				lang = cpp();
				break;
			case 'markup':
				lang = html();
				break;
			case 'javascript':
				lang = javascript({jsx:true});
				break;
			case 'typescript':
				lang = javascript({jsx:true,typescript:true});
				break;
			case 'python':
				lang = python();
				break;
			case 'markdown':
			case 'treeview':
				lang = markdown();
				break;
			case 'sql':
				lang = sql();
				break;
			case 'css':
				lang = StreamLanguage.define(css);
				break;
			case 'less':
				lang = StreamLanguage.define(less);
				break;
			case 'java':
				lang = java();
				break;
			case 'sass':
				lang = StreamLanguage.define(sass);
				break;
			case 'ruby':
			case 'haml':
				lang = StreamLanguage.define(ruby);
				break;
			case 'c':
				lang = StreamLanguage.define(c);
				break;
			case 'csharp':
				lang = StreamLanguage.define(csharp);
				break;
			case 'bash':
			case 'git':
				lang = StreamLanguage.define(shell);
				break;
			case 'coffeescript':
				lang = StreamLanguage.define(coffeeScript);
				break;
			case 'haskell':
				lang = StreamLanguage.define(haskell);
				break;
			case 'apacheconf':
				lang = StreamLanguage.define(xml);
				break;
		}
		setLang( lang );
	}, [props.language])

	return (
		<CodeMirror
			value={props.code}
			minHeight="250px"
			onChange={props.onChange}
			basicSetup={{
				lineNumbers: props.showlines,
			}}
			theme={myTheme}
			extensions={[lang]}
			/>
	);
}
