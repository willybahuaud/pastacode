
import { __ } from '@wordpress/i18n';
import './editor.scss';
import { InspectorControls, BlockControls, useBlockProps } from '@wordpress/block-editor';
import { Fragment, useState, useEffect, useRef } from '@wordpress/element';
import { 
	PanelBody,
	ToggleControl, 
	SelectControl, 
	TextControl,
 } from '@wordpress/components';
const { fields, langs, services, posInfo } = pastaGutVars;
import CodePanel from './codepanel';
import CodeInfo from './codeinfo';

export default function Edit( props ) {
	function camelCase(str) {
		return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function(word, index)
		{
			return index == 0 ? word.toLowerCase() : word.toUpperCase();
		}).replace(/\s+/g, '');
	}

	const codeWrapper = useRef( null );
	const { attributes, setAttributes } = props;
	const [ code, setRemoteCode ] = useState(false);
	const [ loading, setLoading ] = useState(false);
	const blockProps = useBlockProps();

	let classPre = 'code-embed-pre language-' + attributes.lang;
	let classCode = 'code-embed-code language-' + attributes.lang;
	if ( attributes.linenumbers ) {
		classPre += ' line-numbers';
	}
	// Filtrer les fields
	const tf = [];
	for (const [key, value] of Object.entries(fields)) {
		if ( value.classes.includes(attributes.provider) ) {
			if ( ! ['manual','pastacode-highlight','pastacode-lines'].includes( key ) ) {
				const kcamel = camelCase( value.name );
				tf.push( <TextControl
					label={value.label}
					placeholder={value.placeholder}
					value={ attributes[ kcamel ] }
					onChange={ (v) => { setAttributes( { [kcamel]: v } ) } }
				/> )
			}
		}
	}

	/**
	 * Get Code on change
	 */
	useEffect( () => {
		if ( ! loading ) {
			setLoading( true );
			setRemoteCode( false );
			wp.apiFetch( {
				path: 'pastacode/v2/retrieve_code',
				method: 'POST',
				data:{
					provider:attributes.provider,
					lines:attributes.lines,
					file:attributes.file,
					message:attributes.message,
					revision:attributes.revision,
					path_id:attributes.path_id,
					repos:attributes.repos,
					user:attributes.user,
				}
			} ).then( ( res ) => {
				setRemoteCode( res );
				setLoading(false);
			} );
		}
	}, [
		attributes.provider,
		attributes.lines,
		attributes.file,
		attributes.message,
		attributes.revision,
		attributes.path_id,
		attributes.repos,
		attributes.user
	])

	const regex = new RegExp('([0-9-,]+)', 'gm')
	let lineHighlight = '';
	if ( regex.exec(attributes.highlight) !== null ) {
		lineHighlight = attributes.highlight;
	}

	const codeToShow = attributes.provider === 'manual' ? attributes.manual : (code?.code || __('Chargement en cours'))

	useEffect( () => {
		if ( (! props.isSelected) && codeToShow && codeToShow !== __('Chargement en cours') ) {
			Prism.highlightAllUnder( codeWrapper.current );
		}
	})

	return (
		<Fragment>
				<InspectorControls>
					<PanelBody title={ __( 'Réglage de la source', 'capitainewp-gut-bases' ) }>
					<SelectControl
						label="Origine du code"
						value={ attributes.provider }
						options={ services }
						onChange={ (v) => setAttributes( { provider: v } ) }
						__nextHasNoMarginBottom
					/>
					{
						attributes.provider !== 'manual' && (
							<TextControl
								label="Lignes visibles"
								value={ attributes.lines }
								onChange={ (v) => setAttributes( { lines: v } ) }
							/>
						)
					}
					</PanelBody>
					<PanelBody title={ __( 'Options d’affichage', 'capitainewp-gut-bases' ) }>
						<SelectControl
							label="Language"
							value={ attributes.lang }
							options={ langs }
							onChange={ (v) => setAttributes( { lang: v } ) }
							__nextHasNoMarginBottom
						/>
						<TextControl
								label="Lignes soulignées"
								value={ attributes.highlight }
								placeholder="1-4,10…"
								onChange={ (v) => setAttributes( { highlight: v } ) }
							/>
						<ToggleControl
							label="Afficher les numéros de ligne"
							checked={ attributes.linenumbers }
							onChange={ (v) => setAttributes( { linenumbers: v }  )}
						/>
					</PanelBody>
				</InspectorControls>

				{ props.isSelected ? (
					attributes.provider == 'manual' ? (
						<div {...blockProps}>
							<div className="blockcode-settings__wrapper">
								{
									tf
								}
								</div>
							<CodePanel
								code={attributes.manual}
								language={attributes.lang}
								showlines={attributes.linenumbers}
								onChange={(v) => setAttributes( { manual: v })}/>
						</div>
					) : (
						<div {...blockProps}>
							<div className="blockcode-settings__wrapper">
							{
								tf
							}
							</div>
						</div>
					)
				) : (
				<div {...blockProps}>
					<div className="code-embed-wrapper" ref={codeWrapper}>
						{ posInfo == 'top' && <CodeInfo code={code} message={attributes.message}/> }
						<pre className={classPre} data-start={
							( code && code.start ) ? code.start : '1'
						} data-line-offset={
							( code && code.start ) ? code.start - 1 : '0'
						}
						data-line={lineHighlight}><code className={classCode}>{
							codeToShow.replace( /&amp;/g, '&').replace(/&lt;/g, '<' ).replace(/&gt;/g, '>').replace(/&quot;/g, '"').replace(/&#34;/g, '"').replace(/&#039;/g, "'")
						}</code></pre>
						{ posInfo == 'bottom' && <CodeInfo code={code} message={attributes.message}/> }
					</div>
				</div>
				)
				}
			</Fragment>
	);
}
